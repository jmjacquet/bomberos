# -*- coding: utf-8 -*-
from django.conf.urls import *
from bomberos.views import *
from bomberos.models import *
#from django.views.generic import TemplateView
#from django.contrib.auth.decorators import login_required
from exportar import exportarCSV,exportar_xls,exportar_xls_cadetes,exportar_xls_bomberos

urlpatterns = patterns('',
        #url(r'^$','bomberos.views.principal'),
        url(r'^$', PrincipalView.as_view()),

        # Listados
        url(r'bomberos/$', ListBomberosView.as_view(),name='bomberos_list'),
        url(r'asociaciones/$', ListAsociacionesView.as_view(),name='asociaciones_list'),
        url(r'pb/$', ListPadronBView.as_view(),name='pbomb_list'),
        url(r'padron_cadetes/$', ListPadronCView.as_view(),name='padron_cadetes_list'),
        url(r'unidades/$', ListUnidadesView.as_view(),name='unidades_list'),
        url(r'personas/$', personas,name='personas_list'),

        #ABM Bomberos bomberos/<id_asociac>/<accion>
        url(r'bomberos/(?P<asociacion_id>\d{1,5})/$', ListBomberosView.as_view(),name='bomberos_list'),
        #url(r'bomberos/(?P<pk>\d{1,5})/detalle/$',  BomberoDetailView.as_view(), name='bombero_detail'),
        url(r'bomberos/(?P<pk>\d{1,5})/detalle/$',  detalle_Bombero, name='bombero_detail'),
        url(r'bomberos/(?P<asociacion_id>\d{1,5})/agregar/$', BomberoCreateView.as_view(), name='bombero_create'),
        url(r'bomberos/(?P<asociacion_id>\d{1,5})/editar/(?P<pk>\d{1,5})/$', BomberoUpdateView.as_view(), name='bombero_update'),
        url(r'bomberos/(?P<pk>\d{1,5})/baja/$', darBajaBombero, name='bombero_baja'),
        url(r'bomberos/(?P<asociacion_id>\d{1,5})/exportar/CSV/$', exportarCSV,{"modelo": Bombero}, name='bombero_exportarCSV'),
        url(r'bomberos/(?P<asociacion_id>\d{1,5})/exportar/XLS/$',exportar_xls,{"modelo": Bombero}, name='exportar_xls_bomberos'),

        #ABM Asociaciones
        url(r'asociaciones/(?P<asociacion_id>\d{1,5})/$', ListAsociacionesView.as_view(),name='asociaciones_list'),
        url(r'asociaciones/(?P<pk>\d{1,5})/detalle/$',  detalle_Asociacion, name='asociacion_detail'),
        url(r'asociaciones/agregar/$', AsociacionCreateView.as_view(), name='asociacion_create'),
        url(r'asociaciones/(?P<pk>\d{1,5})/editar/$', AsociacionUpdateView.as_view(), name='asociacion_update'),
        #url(r'asociaciones/(?P<pk>\d{1,5})/baja/$', darBaja, name='asociacion_baja'),
        url(r'asociaciones/(?P<asociacion_id>\d{1,5})/exportar/CSV/$', exportarCSV,{"modelo": Asociacion}, name='asociacion_exportarCSV'),
        url(r'asociaciones/(?P<asociacion_id>\d{1,5})/exportar/XLS/$', exportar_xls,{"modelo": Asociacion}, name='asociacion_exportarXLS'),

        #ABM Unidades
        url(r'unidades/(?P<asociacion_id>\d{1,5})/$', ListUnidadesView.as_view(),name='unidades_list'),
        url(r'unidades/(?P<pk>\d{1,5})/detalle/$',  detalle_Unidad, name='unidad_detail'),
        url(r'unidades/(?P<asociacion_id>\d{1,5})/agregar/$', UnidadCreateView.as_view(), name='unidad_create'),
        url(r'unidades/(?P<asociacion_id>\d{1,5})/editar/(?P<pk>\d{1,5})/$', UnidadUpdateView.as_view(), name='unidad_update'),
        url(r'unidades/(?P<pk>\d{1,5})/baja/$', darBajaUnidad,name='unidad_baja'),
        url(r'unidades/exportar/CSV/$', exportarCSV,{"modelo": Unidad}, name='unidad_exportarCSV'),
        url(r'unidades/(?P<asociacion_id>\d{1,5})/exportar/XLS/$', exportar_xls,{"modelo": Unidad}, name='unidad_exportarXLS'),

        #ABM Padron Bomberos
        url(r'pb/(?P<asociacion_id>\d{1,5})/$', ListPadronBView.as_view(),name='pbomb_list'),
        url(r'pb/(?P<pk>\d{1,5})/detalle/$',  PadronBDetailView.as_view(), name='pbomb_detail'),
        url(r'pb/(?P<asociacion_id>\d{1,5})/agregar/$', PadronBCreateView.as_view(), name='pbomb_create'),
        url(r'pb/(?P<asociacion_id>\d{1,5})/editar/(?P<pk>\d{1,5})/$', PadronBUpdateView.as_view(), name='pbomb_update'),
        url(r'pb/(?P<asociacion_id>\d{1,5})/exportar/CSV/$', exportarCSV,{"modelo": PadronBomberos}, name='pbomb_exportarCSV'),
        url(r'pb/(?P<asociacion_id>\d{1,5})/exportar/XLS/$', exportar_xls_bomberos,name='pbomb_exportarXLS'),

        #ABM Padron Cadetes
        url(r'padron_cadetes/(?P<asociacion_id>\d{1,5})/$', ListPadronCView.as_view(),name='padron_cadetes_list'),
        url(r'padron_cadetes/(?P<pk>\d{1,5})/detalle/$',  PadronCDetailView.as_view(), name='padron_cadetes_detail'),
        url(r'padron_cadetes/(?P<asociacion_id>\d{1,5})/agregar/$', PadronCCreateView.as_view(), name='padron_cadetes_create'),
        url(r'padron_cadetes/(?P<asociacion_id>\d{1,5})/editar/(?P<pk>\d{1,5})/$', PadronCUpdateView.as_view(), name='padron_cadetes_update'),
        url(r'padron_cadetes/(?P<asociacion_id>\d{1,5})/exportar/CSV/$', exportarCSV,{"modelo": PadronCadetes}, name='padron_cadetesn_exportarCSV'),
        url(r'padron_cadetes/(?P<asociacion_id>\d{1,5})/exportar/XLS/$', exportar_xls_cadetes, name='padron_cadetes_exportarXLS'),

        # ABM Personas
        url(r'personas/(?P<pk>\d{1,5})/detalle/$',  PersonasDetailView.as_view(), name='personas_detail'),
        url(r'personas/agregar/$', PersonasCreateView.as_view(), name='personas_create'),
        url(r'personas/(?P<pk>\d{1,5})/editar/$', PersonasUpdateView.as_view(), name='personas_update'),
        url(r'personas/(?P<pk>\d{1,5})/baja/$', darBajaPersona, name='personas_baja'),
        #url(r'personas/(?P<asociacion_id>\d{1,5})/exportar/CSV/$', exportarCSV,{"modelo": Persona}, name='personas_exportarCSV'),
        #url(r'personas/(?P<asociacion_id>\d{1,5})/exportar/XLS/$', exportar_xls,{"modelo": Persona}, name='asociacion_exportarXLS'),

)
