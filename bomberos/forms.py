# -*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm
from models import *
from datetime import date



class LoginForm(forms.Form):
    "Formulario de inicio de sesion"
    username = forms.CharField(max_length=80, label='Usuario:')
    password = forms.CharField(max_length=80, label='Password:',
                               widget=forms.PasswordInput(render_value=False))


class BomberoForm(ModelForm):
    #"Formulario de Bombero"
    class Meta:
        model = Bombero
        widgets = {
            'fecha_nac': forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
            'observaciones': forms.Textarea(
                        attrs={'width': '300px',}),
        }
    def __init__(self,*args,**kwargs):
        asoc = kwargs.pop('asociacion_id')
        super (BomberoForm,self ).__init__(*args,**kwargs) # populates the post
        if asoc==None:
                self.fields['asociacion'].queryset = Asociacion.objects.filter(baja=False)
        else:
                self.fields['asociacion'].queryset = Asociacion.objects.filter(asociacion_id= asoc).filter(baja=False)



    #def clean_fechayhora(self):
        #hoy = date.today()
        #fecha = self.cleaned_data['fechayhora']
        #if fecha:
            #if fecha > hoy:
                #raise forms.ValidationError('Fecha incorrecta!!')
        #return fecha

class UnidadForm(ModelForm):
    #"Formulario de Bombero"
    class Meta:
        model = Unidad
        widgets = {
            'observaciones': forms.Textarea(
                        attrs={'width': '300px',}),
        }
    def __init__(self,*args,**kwargs):
        asoc = kwargs.pop('asociacion_id')
        super (UnidadForm,self ).__init__(*args,**kwargs) # populates the post
        if asoc==None:
                self.fields['asociacion'].queryset = Asociacion.objects.filter(baja=False)
        else:
                self.fields['asociacion'].queryset = Asociacion.objects.filter(asociacion_id= asoc).filter(baja=False)




class PadronBomberosForm(ModelForm):
    #"Formulario de Persona"
    def __init__(self,*args,**kwargs):
        asoc = kwargs.pop('asociacion_id')
        super (PadronBomberosForm,self ).__init__(*args,**kwargs) # populates the post
        self.fields['bombero'].queryset = Bombero.objects.filter(baja=False).filter(es_cadete=False)

        if asoc==None:
                self.fields['asociacion'].queryset = Asociacion.objects.filter(baja=False)
        else:
                self.fields['asociacion'].queryset = Asociacion.objects.filter(asociacion_id= asoc).filter(baja=False)
                asociac = Asociacion.objects.get(asociacion_id=asoc)
                self.fields['credencial'] = '%s--R%s' % (asociac.nro_asoc,asociac.nro_regional)

        #self.fields[''].queryset = Municipio.objects.filter(baja=False)


    class Meta:
        model = PadronBomberos
        widgets = {
            'ultimo_ascenso': forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
            'fecha_ingreso': forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
            'reincorporaciones':forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
        }

    def clean_fecha_ingreso(self):
        hoy = date.today()
        fecha = self.cleaned_data['fecha_ingreso']
        if fecha:
            if fecha > hoy:
                raise forms.ValidationError('La fecha debe ser menor a la de hoy.')
        return fecha

    #def clean_detalle(self):
        #det = self.cleaned_data['detalle']
        #if not det:
                #raise forms.ValidationError('Debe cargar alguna descripción!!')
        #return det

class PadronCadetesForm(ModelForm):
    #"Formulario de Persona"
    def __init__(self,*args,**kwargs):
        asoc = kwargs.pop('asociacion_id')

        super (PadronCadetesForm,self ).__init__(*args,**kwargs) # populates the post
        if asoc==None or asoc==0:
            self.fields['bombero'].queryset = Bombero.objects.filter(baja=False).filter(es_cadete=True)
        else:
            self.fields['bombero'].queryset = Bombero.objects.filter(baja=False).filter(es_cadete=True).filter(asociacion=asoc)
        if asoc==None or asoc==0:
            self.fields['asociacion'].queryset = Asociacion.objects.filter(baja=False)
        else:
            self.fields['asociacion'].queryset = Asociacion.objects.filter(asociacion_id=asoc).filter(baja=False)



    class Meta:
        model = PadronCadetes
        widgets = {
            'ultimo_ascenso': forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
            'fecha_ingreso': forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
            'reincorporaciones':forms.DateInput(format='%d/%m/%Y',
                        attrs={'class': 'datePicker',}),
            'observaciones': forms.Textarea(
                        attrs={'width': '300px',}),
        }

    def clean_fecha_ingreso(self):
        hoy = date.today()
        fecha = self.cleaned_data['fecha_ingreso']
        if fecha:
            if fecha > hoy:
                raise forms.ValidationError('La fecha debe ser menor a la de hoy.')
        return fecha

    #def clean_detalle(self):
        #det = self.cleaned_data['detalle']
        #if not det:
                #raise forms.ValidationError('Debe cargar alguna descripción!!')
        #return det

