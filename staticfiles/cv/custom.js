$(document).ready(function(){

	var delays = [600, 900, 1200, 1500, 1800, 2100];

	$("#graph li").each(function( idx ) {
    	var length = $(this).attr("class");
    	$(this).delay( delays[ idx ] ).animate(
    		{'width':length}, {
    			duration:2000,
    			easing: 'easeOutBounce'
    		});
    	$(this).find("span").delay(1800).fadeIn(2000);
  	});
  	
  	
});