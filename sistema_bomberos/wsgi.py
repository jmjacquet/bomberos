import os
import site
import sys
sys.path.insert(0, '/home/grupogua/webapps/django5/sistema_bomberos')

site.addsitedir('/home/grupogua/webapps/django5/lib/python2.7')

site.addsitedir('/home/grupogua/webapps/django5')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sistema_bomberos.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
