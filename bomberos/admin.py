from django.contrib import admin
from models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User



class UsuariosInline(admin.StackedInline):
    model = Usuarios
    fk_name = 'user'
    max_num = 1


class CustomUserAdmin(UserAdmin):
    inlines = [UsuariosInline,]


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

admin.site.register(Localidad)
admin.site.register(Provincia)
admin.site.register(Pais)
admin.site.register(Persona)

class BomberoAdmin(admin.ModelAdmin):
    list_display = ('bombero_id','asociacion','apellido','nombre','direccion','localidad','es_cadete','baja')
    list_filter = ('asociacion','es_cadete',)
    ordering = ('bombero_id',)

admin.site.register(Bombero,BomberoAdmin)
admin.site.register(Asociacion)
admin.site.register(Grado)
admin.site.register(Cargo)
admin.site.register(PadronBomberos)
admin.site.register(PadronCadetes)
admin.site.register(Tipo_Vehiculo)
admin.site.register(Tipo_Carroceria)
admin.site.register(Tipo_Unidad)
admin.site.register(Unidad)
