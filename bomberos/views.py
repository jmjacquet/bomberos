# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.shortcuts import *
from settings import *
from bomberos.models import Bombero,Asociacion,PadronBomberos,PadronCadetes,Unidad,Persona
from django.views.generic import ListView,DetailView,CreateView,UpdateView,TemplateView
from bomberos.forms import BomberoForm,PadronBomberosForm,PadronCadetesForm,UnidadForm
from django.views.generic.edit import ModelFormMixin
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist

#from django.core.mail import send_mail
#from django.views.generic import FormView


def login(request):
    error = None
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
          if user.is_active:
            django_login(request, user)
            # success
            return HttpResponseRedirect(LOGIN_REDIRECT_URL)
          else:
          ## invalid login
           error = "Usuario/Password incorrectos."
        else:
          ## invalid login
           error = "Usuario/Password incorrectos."
          #return direct_to_template(request, 'invalid_login.html')
    return render_to_response('index.html', {'root_url': ROOT_URL,
                                                     'error': error},context_instance=RequestContext(request))

def logout(request):
    request.session.clear()
    django_logout(request)
    return HttpResponseRedirect(ROOT_URL)

def principal(request):
    return HttpResponseRedirect(LOGIN_REDIRECT_URL)

def cv(request):
    return render_to_response('cv.html')

#Principal

class PrincipalView(TemplateView):
    template_name = 'index.html'
    context_object_name = 'asociaciones'

    def get_context_data(self, **kwargs):
        context = super(PrincipalView, self).get_context_data(**kwargs)
        if self.request.user.is_active:
            if self.request.user.get_profile().asociacion==None:
                context['asociaciones'] = Asociacion.objects.filter(baja=False)
            else:
                context['asociaciones'] = Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)

        return context

# Bomberos

class BomberoMixin(object):
    model = Bombero
    form_class = BomberoForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(ROOT_URL)
        return super(BomberoMixin, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.request.user.get_profile().asociacion==None:
            return reverse('bomberos_list',)
        else:
            return reverse('bomberos_list',kwargs={'asociacion_id':self.request.user.get_profile().asociacion_id})

    def get_context_data(self, **kwargs):
        context = super(BomberoMixin, self).get_context_data(**kwargs)
        if self.request.user.is_active:
            if self.request.user.get_profile().asociacion==None:
                context['asociaciones'] = Asociacion.objects.filter(baja=False)
            else:
                context['asociaciones'] = Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)
        try:
            a  = Asociacion.objects.get(pk=self.kwargs.get('asociacion_id'))
        except ObjectDoesNotExist:
            a = None
        context['asociacion'] = a

        return context

    def form_valid(self, form):
        # save but don't commit the model form
        self.object = form.save(commit=False)
        # set the owner to be the current user
        #self.object.owned_by = self.request.user
        self.object.save()
        # ok now call the base class and we are done.
        return super(ModelFormMixin, self).form_valid(form)

class ListBomberosView(BomberoMixin,ListView):
    model = Bombero
    context_object_name = 'bomberos'

    def get_queryset(self):
        asoc=self.kwargs.get('asociacion_id')
        asoc_usr=self.request.user.get_profile().asociacion_id
        if asoc_usr==None:
            if asoc==None or asoc==0:
               return Bombero.objects.filter(baja=False)
            else:
                return Bombero.objects.filter(asociacion=asoc).filter(baja=False)
        elif asoc==None or asoc==0:
            return Bombero.objects.none()
        else:
            return Bombero.objects.filter(asociacion=asoc_usr).filter(baja=False)
class BomberoDetailView(BomberoMixin,DetailView):
    model = Bombero
    context_object_name = 'bomberos'

class BomberoCreateView(BomberoMixin,CreateView):
    model = Bombero
    context_object_name = 'bomberos'
    def get_form_kwargs(self):
        kwargs = super(CreateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs

class BomberoUpdateView(BomberoMixin,UpdateView):
    model = Bombero
    context_object_name = 'bomberos'
    def get_form_kwargs(self):
        kwargs = super(UpdateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs

def darBajaBombero(request,pk):
    obj = get_object_or_404(Bombero,pk=pk)
    obj.baja = 'True'
    obj.save()
    return HttpResponseRedirect(reverse('bomberos_list'))

def detalle_Bombero(request, pk):
    try:
        bomb = Bombero.objects.get(pk=pk)
    except Bombero.DoesNotExist:
        raise Http404
    return render_to_response('bomberos/bombero_detail.html',{'bomberos':bomb},context_instance=RequestContext(request))

# Asociaciones

class AsociacionMixin(object):
    model = Asociacion
    #form_class = AsociacionForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(ROOT_URL)
        return super(AsociacionMixin, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.request.user.get_profile().asociacion==None:
            return reverse('asociaciones_list',)
        else:
            return reverse('asociaciones_list',kwargs={'asociacion_id':self.request.user.get_profile().asociacion_id})

    def get_context_data(self, **kwargs):
        context = super(AsociacionMixin, self).get_context_data(**kwargs)
        if self.request.user.is_active:
            if self.request.user.get_profile().asociacion==None:
                context['asociaciones'] = Asociacion.objects.filter(baja=False)
            else:
                context['asociaciones'] = Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)
        try:
            a  = Asociacion.objects.get(pk=self.kwargs.get('asociacion_id'))
        except ObjectDoesNotExist:
            a = None
        context['asociacion'] = a
        return context

    def form_valid(self, form):
        # save but don't commit the model form
        self.object = form.save(commit=False)
        # set the owner to be the current user
        #self.object.owned_by = self.request.user
        #
        # Here you can make any other adjustments to the model
        #
        self.object.save()
        # ok now call the base class and we are done.
        return super(ModelFormMixin, self).form_valid(form)

class ListAsociacionesView(AsociacionMixin,ListView):
    model = Asociacion
    context_object_name = 'asociaciones'

    def get_queryset(self):
        if self.request.user.get_profile().asociacion==None:
            return Asociacion.objects.filter(baja=False)
        else:
            return Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)
class AsociacionDetailView(AsociacionMixin,DetailView):
    model = Asociacion
    context_object_name = 'asociaciones'
class AsociacionCreateView(AsociacionMixin,CreateView):
    model = Asociacion
    context_object_name = 'asociaciones'
class AsociacionUpdateView(AsociacionMixin,UpdateView):
    model = Asociacion
    context_object_name = 'asociaciones'

def detalle_Asociacion(request, pk):
    try:
        asoc = Asociacion.objects.get(pk=pk)
    except Asociacion.DoesNotExist:
        raise Http404
    return render_to_response('bomberos/asociacion_detail.html',{'asociaciones':asoc},context_instance=RequestContext(request))

def darBajaAsociacion(request,pk):
    obj = get_object_or_404(Asociacion,pk=pk)
    obj.baja = 'True'
    obj.save()
    return HttpResponseRedirect(reverse('asociaciones_list'))


# Unidades
class UnidadMixin(object):
    model = Unidad
    form_class = UnidadForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(ROOT_URL)
        return super(UnidadMixin, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.request.user.get_profile().asociacion==None:
            return reverse('unidades_list',)
        else:
            return reverse('unidades_list',kwargs={'asociacion_id':self.request.user.get_profile().asociacion_id})

    def get_context_data(self, **kwargs):
        context = super(UnidadMixin, self).get_context_data(**kwargs)
        if self.request.user.is_active:
            if self.request.user.get_profile().asociacion==None:
                context['asociaciones'] = Asociacion.objects.filter(baja=False)
            else:
                context['asociaciones'] = Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)
        try:
            a  = Asociacion.objects.get(pk=self.kwargs.get('asociacion_id'))
        except ObjectDoesNotExist:
            a = None
        context['asociacion'] = a

        return context


    def form_valid(self, form):
        # save but don't commit the model form
        self.object = form.save(commit=False)
        # set the owner to be the current user
        #self.object.owned_by = self.request.user
        #
        # Here you can make any other adjustments to the model
        #
        self.object.save()
        # ok now call the base class and we are done.
        return super(ModelFormMixin, self).form_valid(form)

class ListUnidadesView(UnidadMixin,ListView):
    model = Unidad
    context_object_name = 'unidades'

    def get_queryset(self):
        asoc=self.kwargs.get('asociacion_id')
        asoc_usr=self.request.user.get_profile().asociacion_id

        if asoc_usr==None:
            if asoc==None:
               return Unidad.objects.all()
            else:
                return Unidad.objects.filter(asociacion=asoc)
        elif asoc==None:
            return Unidad.objects.none()
        else:
            return Unidad.objects.filter(asociacion=asoc_usr)

class UnidadDetailView(UnidadMixin,DetailView):
    model = Unidad
    context_object_name = 'unidades'
class UnidadCreateView(UnidadMixin,CreateView):
    model = Unidad
    context_object_name = 'unidades'
    def get_form_kwargs(self):
        kwargs = super(CreateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs
class UnidadUpdateView(UnidadMixin,UpdateView):
    model = Unidad
    context_object_name = 'unidades'
    def get_form_kwargs(self):
        kwargs = super(UpdateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs

def darBajaUnidad(request,pk):
    obj = get_object_or_404(Unidad,pk=pk)
    obj.baja = 'True'
    obj.save()
    return HttpResponseRedirect(reverse('unidades_list'))

def detalle_Unidad(request, pk):
    try:
        unid = Unidad.objects.get(pk=pk)
    except Unidad.DoesNotExist:
        raise Http404
    return render_to_response('bomberos/unidad_detail.html',{'unidades':unid},context_instance=RequestContext(request))



# Padron Bomberos
class PadronBMixin(object):
    model = PadronBomberos
    form_class = PadronBomberosForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(ROOT_URL)
        return super(PadronBMixin, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.request.user.get_profile().asociacion==None:
            return reverse('pbomb_list',)
        else:
            return reverse('pbomb_list',kwargs={'asociacion_id':self.request.user.get_profile().asociacion_id})

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return super(ModelFormMixin, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PadronBMixin, self).get_context_data(**kwargs)
        if self.request.user.is_active:
            if self.request.user.get_profile().asociacion==None:
                context['asociaciones'] = Asociacion.objects.filter(baja=False)
            else:
                context['asociaciones'] = Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)
        try:
            a  = Asociacion.objects.get(asociacion_id=self.kwargs.get('asociacion_id'))
        except ObjectDoesNotExist:
            a = None
        context['asociacion'] = a

        return context

class ListPadronBView(PadronBMixin,ListView):
    model = PadronBomberos

    def get_queryset(self):
        asoc=self.kwargs.get('asociacion_id')
        asoc_usr=self.request.user.get_profile().asociacion_id

        if asoc_usr==None:
            if asoc==None:
               return PadronBomberos.objects.all()
            else:
                return PadronBomberos.objects.filter(asociacion=asoc)
        elif asoc==None:
            return PadronBomberos.objects.none()
        else:
            return PadronBomberos.objects.filter(asociacion=asoc_usr)

class PadronBDetailView(PadronBMixin,DetailView):
    model = PadronBomberos
    context_object_name = 'padron'

class PadronBCreateView(PadronBMixin,CreateView):
    model = PadronBomberos
    context_object_name = 'padron'
    def get_form_kwargs(self):
        kwargs = super(CreateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs

class PadronBUpdateView(PadronBMixin,UpdateView):
    model = PadronBomberos
    context_object_name = 'padron'
    def get_form_kwargs(self):
        kwargs = super(UpdateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs



# Padron Cadetes
class PadronCMixin(object):
    model = PadronCadetes
    form_class = PadronCadetesForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(ROOT_URL)
        return super(PadronCMixin, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.request.user.get_profile().asociacion==None:
            return reverse('padron_cadetes_list',)
        else:
            return reverse('padron_cadetes_list',kwargs={'asociacion_id':self.request.user.get_profile().asociacion_id})

    def form_valid(self, form):
        # save but don't commit the model form
        self.object = form.save(commit=False)
        # set the owner to be the current user
        #self.object.owned_by = self.request.user
        #
        # Me trae por defecto la asociacion actual del bombero
        self.object.save()
        # ok now call the base class and we are done.
        return super(ModelFormMixin, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PadronCMixin, self).get_context_data(**kwargs)
        if self.request.user.is_active:
            if self.request.user.get_profile().asociacion==None:
                context['asociaciones'] = Asociacion.objects.filter(baja=False)
            else:
                context['asociaciones'] = Asociacion.objects.filter(asociacion_id=self.request.user.get_profile().asociacion_id).filter(baja=False)
        try:
            a  = Asociacion.objects.get(asociacion_id=self.kwargs.get('asociacion_id'))
        except ObjectDoesNotExist:
            a = None
        context['asociacion'] = a

        return context

class ListPadronCView(PadronCMixin,ListView):
    model = PadronCadetes
    context_object_name = 'padron'
    def get_queryset(self):
        asoc=self.kwargs.get('asociacion_id')
        asoc_usr=self.request.user.get_profile().asociacion_id

        if asoc_usr==None:
            if asoc==None:
               return PadronCadetes.objects.all()
            else:
                return PadronCadetes.objects.filter(asociacion=asoc)
        elif asoc==None:
            return PadronCadetes.objects.none()
        else:
            return PadronCadetes.objects.filter(asociacion=asoc_usr)
class PadronCDetailView(PadronCMixin,DetailView):
    model = PadronCadetes
    context_object_name = 'padron'
class PadronCCreateView(PadronCMixin,CreateView):
    model = PadronCadetes
    context_object_name = 'padron'
    def get_form_kwargs(self):
        kwargs = super(CreateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs
class PadronCUpdateView(PadronCMixin,UpdateView):
    model = PadronCadetes
    context_object_name = 'padron'
    def get_form_kwargs(self):
        kwargs = super(UpdateView, self).get_form_kwargs()
        kwargs['asociacion_id'] = self.request.user.get_profile().asociacion_id
        return kwargs

# Personas
class PersonaMixin(object):
    model = Persona

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(ROOT_URL)
        return super(PersonaMixin, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('personas_list',)


class ListPersonasView(PersonaMixin,ListView):
    model = Persona
    context_object_name = 'persona'
    def get_queryset(self):
        return Persona.objects.all()

class PersonasDetailView(PersonaMixin,DetailView):
    model = Persona
    context_object_name = 'persona'
class PersonasCreateView(PersonaMixin,CreateView):
    model = Persona
    context_object_name = 'persona'

class PersonasUpdateView(PersonaMixin,UpdateView):
    model = Persona
    context_object_name = 'persona'

def darBajaPersona(request,pk):
    obj = get_object_or_404(Persona,pk=pk)
    obj.baja = 'True'
    obj.save()
    return HttpResponseRedirect(reverse('personas_list'))


def personas(request):
    personas = Persona.objects.all()
    if request.is_ajax():
        filtro = request.GET.get('filtro')
        if  filtro != '':
            new_list = []
            new_list.append(render_to_string('bomberos/personas.html', {'personas': personas.order_by('filtro')}))
            json =  simplejson.dumps(new_list, cls=DjangoJSONEncoder)
            return HttpResponse(json, mimetype='application/json')
    return render(request, "bomberos/personas.html",locals())