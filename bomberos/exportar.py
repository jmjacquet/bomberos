# -*- coding: utf-8 -*-
from datetime import datetime, date
from django.http import HttpResponse
from django.template.defaultfilters import slugify
from bomberos.models import Asociacion,PadronBomberos,PadronCadetes
import csv
import xlwt
from xlutils.copy import copy
from xlrd import open_workbook
from django.conf import settings
from xlutils.styles import Styles

def exportarCSV(request,modelo):
    model = modelo

    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s.csv' % slugify(model.__name__)
    response['Content-Type'] = 'text/csv; charset=utf-8'

    writer = csv.writer(response)
    # Write headers to CSV file
    encabezados=[]
    for field in model._meta.fields:
        encabezados.append(field.verbose_name)
    writer.writerow(encabezados)
    # Write data to CSV file
    headers = []
    for field in model._meta.fields:
        headers.append(field.name)
    qs = model.objects.all()
    for obj in qs:
        row = []
        for field in headers:
            if field in headers:
                val = getattr(obj, field)
                if callable(val):
                    val =  val()
                if type(val) == unicode:
                    val = val.encode("utf-8")
                row.append(val)
        writer.writerow(row)
    # Return CSV file to browser as download
    return response

def excelview(request,filename, fields, values_list, save=False, folder=""):
    filename = slugify(filename)
    book = xlwt.Workbook(encoding='utf8')
    sheet = book.add_sheet(filename)

    default_style = xlwt.Style.default_style
    datetime_style = xlwt.easyxf(num_format_str='dd/mm/yyyy hh:mm')
    date_style = xlwt.easyxf(num_format_str='dd/mm/yyyy')
    # Escribe los titulos
    for j, f in enumerate(fields):
        sheet.write(0, j, fields[j])

    # Escribe los datos
    for row, rowdata in enumerate(values_list):
        for col, val in enumerate(rowdata):
            if isinstance(val, datetime):
                style = datetime_style
            elif isinstance(val, date):
                style = date_style
            else:
                style = default_style

            sheet.write(row + 1 , col, val, style=style)


    if not save:
        response = HttpResponse(mimetype='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename
        book.save(response)
        return response
    else:
        dirpath = '%s/%s' % (settings.MEDIA_ROOT, folder)
        if folder != "":
            import os
            if not os.path.exists(dirpath):
                os.makedirs(dirpath)
        filepath = '%s%s.xls' % (dirpath, filename)
        book.save(filepath)
        return "%s%s%s.xls" % (settings.MEDIA_URL, folder, filename)


def exportar_xls(request,modelo,asociacion_id=None):
    if asociacion_id==None:
        queryset = modelo.objects.filter(baja=False)
    else:
        queryset = modelo.objects.filter(baja=False).filter(asociacion=asociacion_id)
    filename = modelo._meta.verbose_name_plural.lower()
    fields = modelo._meta.get_all_field_names()
    try:
        return excelview(request,filename, fields, queryset.values_list(*fields),False,'')
    except Exception, e:
        raise e



def exportar_xls_cadetes(request,asociacion_id=None):
    if asociacion_id==None or asociacion_id==0:
        queryset = PadronCadetes.objects.all()
    else:
        queryset = PadronCadetes.objects.filter(asociacion=asociacion_id)
    filename = PadronCadetes._meta.verbose_name_plural.lower()
    try:
        save=False
        folder=""
        desde=7
        plantilla='/home/grupogua/webapps/django5/sistema_bomberos/archivos/padronCadetes.xls'

        original = open_workbook(plantilla,formatting_info=True)
        original_hoja = original.sheet_by_index(0) # read only copy to introspect the file
        styles = Styles(original)
        book = copy(original) # a writable copy (I can't read values out of this, only write to it)
        sheet = book.get_sheet(0) # the sheet to write to within the writable copy

        default_style = xlwt.easyxf("font: height 200, name Arial; align: vert centre, horiz left;")
        date_style = xlwt.easyxf(num_format_str="dd/mm/yyyy")

        # Escribe los datos
        for nfila,fila in enumerate(queryset):
            ind=nfila+desde
            sheet.write(ind, 0, fila.bombero.apellido, default_style)
            sheet.write(ind, 1, fila.bombero.nombre, default_style)
            sheet.write(ind, 3, fila.get_nivel_display(), default_style)
            sheet.write(ind, 4, fila.cargo.descripcion, default_style)
            sheet.write(ind, 5, fila.ultimo_ascenso, date_style)
            sheet.write(ind, 6, fila.fecha_ingreso,date_style)
            sheet.write(ind, 7, fila.bombero.pais.nombre, default_style)
            sheet.write(ind, 8, fila.bombero.fecha_nac,date_style)
            sheet.write(ind, 9, fila.bombero.lugar_nac, default_style)
            sheet.write(ind,10, fila.bombero.provincia.nombre, default_style)
            sheet.write(ind,11, fila.bombero.get_estado_civil_display(), default_style)
            sheet.write(ind,12, fila.bombero.nrodoc, default_style)
            sheet.write(ind,13, fila.bombero.get_grupo_sanguineo_display(), default_style)

        #Datos Asociacion
        if asociacion_id==None or asociacion_id==0:
            asoc = Asociacion.objects.get(asociacion_id=request.user.get_profile().asociacion_id)
        else:
            asoc = Asociacion.objects.get(asociacion_id=asociacion_id)

        sheet.write(1, 10, asoc.nombre)
        sheet.write(3, 11, asoc.nombre)
        sheet.write(1, 15, asoc.nro_asoc)
        sheet.write(2, 15, asoc.nro_regional)
        #sheet.write(2, 13, añoPadron)

        if not save:
            response = HttpResponse(mimetype='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename
            book.save(response)
            return response
        else:
            dirpath = '%s/%s' % (settings.MEDIA_ROOT, folder)
            if folder != "":
                import os
                if not os.path.exists(dirpath):
                    os.makedirs(dirpath)
            filepath = '%s%s.xls' % (dirpath, filename)
            book.save(filepath)
            return "%s%s%s.xls" % (settings.MEDIA_URL, folder, filename)
    except Exception, e:
        raise e

def exportar_xls_bomberos(request,asociacion_id=None):
    if asociacion_id==None or asociacion_id==0:
        queryset = PadronBomberos.objects.all()
    else:
        queryset = PadronBomberos.objects.filter(asociacion=asociacion_id)

    filename = PadronBomberos._meta.verbose_name_plural.lower()

    try:
        save=False
        folder=""
        desde=8
        plantilla='/home/grupogua/webapps/django5/sistema_bomberos/archivos/padronBomberos.xls'

        original = open_workbook(plantilla,formatting_info=True)
        original_hoja = original.sheet_by_index(0) # read only copy to introspect the file
        styles = Styles(original)
        book = copy(original) # a writable copy (I can't read values out of this, only write to it)
        sheet = book.get_sheet(0) # the sheet to write to within the writable copy

        default_style = xlwt.easyxf("font: height 200, name Arial; align: vert centre, horiz left;")
        date_style = xlwt.easyxf(num_format_str="dd/mm/yyyy")

        # Escribe los datos
        for nfila,fila in enumerate(queryset):
            ind=nfila+desde
            sheet.write(ind, 1, fila.credencial, default_style)
            sheet.write(ind, 2, fila.bombero.apellido, default_style)
            sheet.write(ind, 3, fila.bombero.nombre, default_style)
            sheet.write(ind, 5, fila.grado.descripcion, default_style)
            sheet.write(ind, 6, fila.cargo.descripcion, default_style)
            sheet.write(ind, 7, fila.ultimo_ascenso, date_style)
            sheet.write(ind, 8, fila.fecha_ingreso,date_style)
            sheet.write(ind, 9, fila.bombero.pais.nombre, default_style)
            sheet.write(ind,10, fila.bombero.fecha_nac,date_style)
            sheet.write(ind,11, fila.bombero.lugar_nac, default_style)
            sheet.write(ind,12, fila.bombero.provincia.nombre, default_style)
            sheet.write(ind,13, fila.bombero.get_estado_civil_display(), default_style)
            sheet.write(ind,14, fila.bombero.nrodoc, default_style)
            sheet.write(ind,15, fila.bombero.get_grupo_sanguineo_display(), default_style)
            if fila.iapos:
                sheet.write(ind,16, '-', default_style)

            sheet.write(ind, 17, fila.reincorporaciones, date_style)

        #Datos Asociacion
        if asociacion_id==None or asociacion_id==0:
            asoc = Asociacion.objects.get(asociacion_id=request.user.get_profile().asociacion_id)
        else:
            asoc = Asociacion.objects.get(asociacion_id=asociacion_id)

        sheet.write(1, 10, asoc.nombre)
        sheet.write(3, 11, asoc.nombre)
        sheet.write(1, 15, asoc.nro_asoc)
        sheet.write(2, 15, asoc.nro_regional)
        #sheet.write(2, 13, añoPadron)

        if not save:
            response = HttpResponse(mimetype='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename
            book.save(response)
            return response
        else:
            dirpath = '%s/%s' % (settings.MEDIA_ROOT, folder)
            if folder != "":
                import os
                if not os.path.exists(dirpath):
                    os.makedirs(dirpath)
            filepath = '%s%s.xls' % (dirpath, filename)
            book.save(filepath)
            return "%s%s%s.xls" % (settings.MEDIA_URL, folder, filename)

    except Exception, e:
        raise e



