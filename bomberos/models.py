# -*- encoding: utf-8 -*-
from django.db import models
from utilidades import *
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

class Localidad(models.Model):
    localidad_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    class Meta:
        db_table = u'Localidad'
        verbose_name_plural = "Localidades"
        ordering = ["-nombre"]
    def __unicode__(self):
        return u'%s' % (self.nombre)

class Provincia(models.Model):
    provincia_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    class Meta:
        db_table = u'Provincia'
        verbose_name_plural = "Provincias"
    def __unicode__(self):
        return u'%s' % (self.nombre)

class Pais(models.Model):
    pais_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    class Meta:
        db_table = u'Pais'
        verbose_name_plural = "Paises"
    def __unicode__(self):
        return u'%s' % (self.nombre)

class Persona(models.Model):
    persona_id = models.AutoField(primary_key=True)
    apellido = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100)
    tipodoc = models.CharField('Tipo Documento',max_length=10,choices=TIPO_DOC,default=1)
    nrodoc = models.CharField('Número Documento',max_length=20)
    direccion = models.CharField(max_length=100, blank=True)
    telefono = models.CharField(max_length=45, blank=True)
    celular = models.CharField(max_length=45, blank=True)
    email = models.EmailField(max_length=45, blank=True)
    baja = models.BooleanField(default=False)

    class Meta:
        db_table = u'Persona'
        verbose_name_plural = "Personas"
    def __unicode__(self):
        return u'%s %s - %s: %s' % (self.apellido, self.nombre,self.tipodoc,self.nrodoc)

    def get_absolute_url(self):
    	return reverse('personas_detail', args=[self.pk])


class Asociacion(models.Model):
    asociacion_id = models.AutoField(primary_key=True)
    nro_asoc = models.CharField(max_length=10)
    nombre = models.CharField(max_length=20)
    nro_regional = models.CharField(max_length=10)
    direccion = models.CharField(max_length=100, blank=True)
    localidad = models.ForeignKey(Localidad, verbose_name = 'Localidad',related_name='+')
    provincia = models.ForeignKey(Provincia, verbose_name = 'Provincia',related_name='+')
    telefono1 = models.CharField(max_length=45, blank=True)
    telefono2 = models.CharField(max_length=45, blank=True)
    telefono3 = models.CharField(max_length=45, blank=True)
    email = models.EmailField(blank=True)
    fax = models.CharField(max_length=45, blank=True)
    presidente = models.ForeignKey(Persona, verbose_name = 'Presidente',related_name='+')
    jefe = models.ForeignKey('Bombero',limit_choices_to = {'es_cadete': False} ,verbose_name = 'Jefe',related_name='+')
    subjefe = models.ForeignKey('Bombero',limit_choices_to = {'es_cadete': False} ,verbose_name = 'Subjefe',related_name='+')
    baja = models.BooleanField(default=False)

    class Meta:
        db_table = u'Asociacion'
        verbose_name_plural = "Asociaciones"

    def get_absolute_url(self):
    	return reverse('asociacion_detail', args=[self.pk])

    def __unicode__(self):
        return u'%s - %s - %s' % (self.nro_asoc,self.nro_regional,self.nombre)


class Bombero(models.Model):
    bombero_id = models.AutoField(primary_key=True)
    asociacion = models.ForeignKey(Asociacion, verbose_name = 'Asociacion',related_name='+',null=True, blank=True)
    apellido = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100, blank=True)
    localidad = models.ForeignKey(Localidad, verbose_name = 'Localidad',related_name='+')
    provincia = models.ForeignKey(Provincia, verbose_name = 'Provincia',related_name='+')
    pais = models.ForeignKey(Pais, verbose_name = 'Pais',related_name='+')
    tipodoc = models.CharField('Tipo Documento',max_length=10,choices=TIPO_DOC,default=1)
    nrodoc = models.CharField('Número Documento',max_length=20)
    fecha_nac = models.DateField()
    lugar_nac = models.CharField(max_length=100, blank=True)
    estado_civil = models.CharField('Estado Civil',max_length=10,choices=TIPO_ESTADO_CIVIL,default=1)
    grupo_sanguineo = models.CharField('Grupo Sanguíneo',max_length=10,choices=TIPO_GS,default=1,blank=False)
    telefono = models.CharField(max_length=45, blank=True)
    celular = models.CharField(max_length=45, blank=True)
    email = models.EmailField(max_length=45, blank=True)
    observaciones = models.TextField(blank=True)
    foto= models.ImageField(blank=True, upload_to='imagenes')
    es_cadete = models.BooleanField(default=False)
    baja = models.BooleanField(default=False)
    class Meta:
        db_table = u'Bombero'
        verbose_name_plural = "Bomberos"


    def get_absolute_url(self):
    	return reverse('bombero_detail', args=[self.pk])

    def __unicode__(self):
        return u'%s %s - %s: %s' % (self.apellido, self.nombre,self.tipodoc,self.nrodoc)


class Grado(models.Model):
    grado_id = models.AutoField(primary_key=True)
    prioridad = models.PositiveIntegerField()
    descripcion = models.CharField(max_length=50)
    class Meta:
        db_table = u'Grado'
        verbose_name_plural = "Grados"
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Cargo(models.Model):
    cargo_id = models.AutoField(primary_key=True)
    prioridad = models.PositiveIntegerField()
    descripcion = models.CharField(max_length=50)
    class Meta:
        db_table = u'Cargo'
        verbose_name_plural = "Cargos"
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class PadronBomberos(models.Model):
    padron_id = models.AutoField(primary_key=True)
    anio = models.PositiveIntegerField(verbose_name = 'Año')
    bombero = models.ForeignKey(Bombero,limit_choices_to = {'es_cadete': False}, verbose_name = 'Bombero Voluntario',related_name='+')
    asociacion = models.ForeignKey(Asociacion, verbose_name = 'Asociacion',related_name='+')
    credencial = models.CharField(max_length=50,
        validators=[
            RegexValidator(
                regex= '[0-9]{1,3}[-][0-9]{1,3}[-][R][0-9]{1,3}',
                message='La credencial debe ser del tipo [Nº Asoc.]-[Nº Bombero]-[R + Nº Regional]',
                code='invalid_credencial'
            ),
        ])

    grado =models.ForeignKey(Grado, verbose_name = 'Grado',related_name='+')
    cargo =models.ForeignKey(Cargo, verbose_name = 'Cargo',related_name='+')
    ultimo_ascenso = models.DateField(null=True, blank=True)
    fecha_ingreso = models.DateField(null=True, blank=False)
    iapos = models.BooleanField(default=False,blank=True)
    reincorporaciones = models.DateField(null=True, blank=True)
    observaciones = models.TextField(blank=True)

    class Meta:
        db_table = 'PadronBomberos'
        verbose_name_plural = "PadronBomberos"
        unique_together = ('anio', 'bombero','asociacion',)

    def get_absolute_url(self):
    	return reverse('pbomb_detail', args=[self.pk])

    def __unicode__(self):
        return u'%s' % (self.credencial)

    def clean(self):
        super(PadronBomberos, self).clean()

class PadronCadetes(models.Model):
    padron_id = models.AutoField(primary_key=True)
    anio = models.PositiveIntegerField(verbose_name = 'Año')
    bombero = models.ForeignKey(Bombero,limit_choices_to = {'es_cadete': True}, verbose_name = 'Bombero Voluntario',related_name='+')
    asociacion = models.ForeignKey(Asociacion, verbose_name = 'Asociacion',related_name='+')
    nivel = models.CharField('Nivel',max_length=10,choices=TIPO_NIVEL,default=1)#En el caso de los cadetes se le carga el nivel
    cargo =models.ForeignKey(Cargo, verbose_name = 'Cargo',related_name='+')
    ultimo_ascenso = models.DateField(null=True, blank=True)
    fecha_ingreso = models.DateField(null=True, blank=False)
    observaciones = models.TextField(blank=True)

    class Meta:
        db_table = u'PadronCadetes'
        verbose_name_plural = "PadronCadetes"
        unique_together = ('anio', 'bombero','asociacion',)

    def get_absolute_url(self):
        return reverse('padron_cadetes_detail', args=[self.pk])

    def __unicode__(self):
        return u'%s' % (self.bombero)

    def cadetesPorId(id):
        if not id:
            return self.objects.all()
        else:
            return self.objects.filter(asociacion__pk=id)

class Tipo_Vehiculo(models.Model):
    vehiculo_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    class Meta:
        db_table = u'Tipo_Vehiculo'
        verbose_name_plural = "Tipo_Vehiculos"
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Tipo_Carroceria(models.Model):
    carroceria_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    class Meta:
        db_table = u'Tipo_Carroceria'
        verbose_name_plural = "Tipo_Carrocerias"
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Tipo_Unidad(models.Model):
    tipo_unidad_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    class Meta:
        db_table = u'Tipo_Unidad'
        verbose_name_plural = "Tipo_Unidades"
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Unidad(models.Model):
    unidad_id = models.AutoField(primary_key=True)
    asociacion = models.ForeignKey(Asociacion, verbose_name = 'Asociacion',related_name='+',null=True, blank=True)
    tipo_vehiculo = models.ForeignKey(Tipo_Vehiculo, verbose_name = 'Tipo Vehículo',related_name='+')
    tipo_carroceria = models.ForeignKey(Tipo_Carroceria, verbose_name = 'Tipo Carrocería',related_name='+')
    marca = models.CharField(verbose_name = 'Marca',max_length=20)
    anio = models.PositiveIntegerField()
    numero_motor = models.CharField(max_length=45)
    numero_chasis = models.CharField(max_length=45)
    dominio = models.CharField(max_length=10)
    litros = models.DecimalField(verbose_name = 'Litros de Agua',max_digits=9, decimal_places=2)
    disp_regional=models.BooleanField(default=False);
    tipo_unidad = models.ForeignKey(Tipo_Unidad, verbose_name = 'Tipo Unidad',related_name='+')
    observaciones = models.TextField(blank=True)
    baja = models.BooleanField(default=False)
    class Meta:
        db_table = u'Unidad'
        verbose_name_plural = "Unidades"

    def __unicode__(self):
        return u'%s' % (self.dominio)

    def get_absolute_url(self):
        return reverse('unidad_detail', args=[self.pk])


class Usuarios(models.Model):
    user = models.OneToOneField(User, unique=True, primary_key=True, related_name="user")
    asociacion = models.ForeignKey(Asociacion, verbose_name = 'Asociacion',related_name='+',null=True, blank=True)

    def __unicode__(self):
        return self.user.username


def create_user_profile(sender, instance, created, **kwargs):
    """Create the UserProfile when a new User is saved"""
    if created:
        profile = Usuarios()
        profile.user = instance
        profile.save()

post_save.connect(create_user_profile, sender=User)