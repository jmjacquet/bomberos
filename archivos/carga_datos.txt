Volcar datos existentes
======================
dumpdata --indent=4 -e contenttypes -e auth  > bomberos.json

Levartar archivo en la BD
========================

loaddata bomberos.json 



Coordenadas Planilla Bomberos:
==============================

K2:11-2 Nombre Asociación
M3:13-2 Año del Padron
P2:16-2 Nº asociacion
P3:16-3 Regional(Romano)
L14:12-4 Nombre Asociación