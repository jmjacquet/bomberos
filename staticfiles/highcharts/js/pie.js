$(function () {
    	
    	// Radialize the colors
		Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
		    return {
		        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
		        stops: [
		            [0, color],
		            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
		        ]
		    };
		});
		
		// Build the chart
        $('#idiomas').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Idiomas que hablo...'
            },
            tooltip: {
        	    pointFormat: '{series.data.name}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',

                }
            },
            series: [{
                type: 'pie',
                name: 'Porcentaje',
                data: [
                    ['Inglés',       30],
                    {
                        name: 'Castellano',
                        y: 50.0,
                        sliced: true,
                        selected: true
                    },
                    ['Alemán',    10],
                    ['Portugués',     10],
                ]
            }]
        });
    });

