# -*- coding: utf-8 *-*

TIPO_DOC = (
    ( 'DNI','DNI'),
    ( 'LE','LE'),
    ( 'LC','LC'),
    ( 'Otro','Otro'),
    )

TIPO_NIVEL = (
    ('1', 'I'),
    ('2', 'II'),
    ('3', 'III'),
    )

TIPO_GS = (
    ('1','A RH+'),
    ('2','A RH-'),
    ('3','B RH+'),
    ('4','B RH-'),
    ('5','AB RH+'),
    ('6','AB RH-'),
    ('7','0 RH+'),
    ('8','0 RH-'),
    )

TIPO_ESTADO_CIVIL = (
    ('1', 'Soltero/a'),
    ('2', 'Casado/a'),
    ('3', 'Divorciado/a'),
    ('4', 'Viudo/a'),
    ('3', 'Separado/a'),
    ('4', 'Otro'),
    )


