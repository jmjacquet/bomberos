# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib import admin
from bomberos.views import *
admin.autodiscover()

urlpatterns = patterns('',


    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'bomberos.views.login'),
    url(r'^logout/$', 'bomberos.views.logout'),
    url(r'^bomberos/', include('bomberos.urls')),
    url(r'^contacto/', include("contact_form.urls",namespace="contact_form")),
    url(r'^$', PrincipalView.as_view()),
    url(r'^cv/','bomberos.views.cv'),

)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
